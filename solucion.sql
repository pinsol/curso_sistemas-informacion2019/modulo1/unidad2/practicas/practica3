﻿                                            /** MODULO I - UNIDAD II - PRACTICA 3 **/

USE practica3;

/*
  1. Visualizar el número de empleados de cada departamento.
  Utilizar GROUP BY para agrupar por departamento
*/
SELECT
  e.dept_no Departamento, COUNT(*) nEmpleados
FROM
  emple e
    GROUP BY
      e.dept_no
;

/*
  2. Visualizar los departamentos con más de 5 empleados.
  Utilizar GROUP BY para agrupar por departamento y HAVING para
  establecer la condición sobre los grupos
*/
SELECT
  e.dept_no Departamento
FROM
  emple e
    GROUP BY e.dept_no
    HAVING COUNT(*) > 5
;

/* 3. Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY) */
SELECT
  e.dept_no Departamento,
  AVG(e.salario) salarioMedio
FROM
  emple e
    GROUP BY e.dept_no
;

/*
  4. Visualizar el nombre de los empleados vendedores del departamento ʻVENTASʼ
  (Nombre del departamento=ʼVENTASʼ, oficio=ʼVENDEDORʼ)
*/
SELECT
  e.apellido
FROM
  emple e
  JOIN (SELECT
          d.dept_no
        FROM
          depart d
        WHERE
          d.dnombre = 'VENTAS'
        ) c1 ON e.dept_no = c1.dept_no
WHERE
  e.oficio = 'VENDEDOR'
;

/*
   5. Visualizar el número de vendedores del departamento ʻVENTASʼ
   (utilizar la función COUNT sobre la consulta anterior)
*/
SELECT
  COUNT(*) nVendedoresenVENTAS 
FROM
  emple e JOIN (
            SELECT
              d.dept_no
            FROM
              depart d
            WHERE
              d.dnombre = 'VENTAS'
) c1 ON e.dept_no=c1.dept_no
;

/* 6. Visualizar los oficios de los empleados del departamento ʻVENTASʼ */
SELECT
  DISTINCT e.oficio 
FROM
  emple e JOIN (
SELECT d.dept_no FROM depart d WHERE d.dnombre = 'VENTAS'
) c1 ON e.dept_no = c1.dept_no
;

/*
  7. A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento
     cuyo oficio sea ʻEMPLEADOʼ (utilizar GROUP BY para agrupar por departamento.
     En la cláusula WHERE habrá que indicar que el oficio es ʻEMPLEADOʼ)
*/
SELECT
  e.dept_no,
  COUNT(*) nEmpleados
FROM
  emple e
WHERE
  e.oficio = 'EMPLEADO'
    GROUP BY e.dept_no
;

/* 8. Visualizar el departamento con más empleados */
SELECT
  c3.dept_no
FROM
  (SELECT
    MAX(c1.nEmpleados) maximo 
  FROM 
    (
    SELECT
      dept_no,
      COUNT(*) nEmpleados
    FROM
      emple e 
        GROUP BY e.dept_no
    ) c1
  ) c2
JOIN
  (
  SELECT
    dept_no,
    COUNT(*) nEmpleados
  FROM
    emple e 
      GROUP BY e.dept_no
  ) c3 ON c3.nEmpleados = c2.maximo
;

/* 9. Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados */
SELECT e.dept_no,SUM(e.salario) suma FROM emple e GROUP BY e.dept_no;   -- suma salarial departamental
SELECT AVG(e.salario) media FROM emple e;     -- media salarial

SELECT
  c1.dept_no departamento
FROM 
  (SELECT e.dept_no,SUM(e.salario) sumaSalarial FROM emple e GROUP BY e.dept_no) c1
WHERE
  sumaSalarial > (SELECT AVG(e.salario) mediaSalarial FROM emple e)
;

/* 10. Para cada oficio obtener la suma de salarios */
SELECT
  e.oficio,
  SUM(e.salario) sumaSalarial
FROM
  emple e
    GROUP BY e.oficio
;

/* 11. Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ */
SELECT
  e.oficio,SUM(e.salario) sumaSalarialVENTAS 
FROM
  (SELECT d.dept_no FROM depart d WHERE d.dnombre = 'VENTAS') c1 
JOIN emple e USING(dept_no) 
  GROUP BY e.oficio
;

/* 12. Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado */
SELECT c4.dept_no 
FROM
  (
    SELECT dept_no, COUNT(*) nEmpleados 
    FROM emple 
    WHERE oficio = 'empleado' 
      GROUP BY dept_no
  ) c3 
JOIN (
    SELECT MAX(c1.nEmpleados) maximo 
    FROM
      (
        SELECT dept_no, COUNT(*) nEmpleados
        FROM emple 
        WHERE oficio = 'empleado' 
          GROUP BY dept_no) c1
      ) c2
ON c2.maximo = c3.nEmpleados
;

/* 13.  Mostrar el número de oficios distintos de cada departamento */
SELECT
  e.dept_no,
  COUNT(DISTINCT e.oficio) nOficiosDistintos
FROM
  emple e
    GROUP BY e.dept_no
;

/* 14. Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión */
SELECT
  c1.dept_no 
FROM 
  (
  SELECT
    e.dept_no,
    e.oficio,
    COUNT(*) nEmpleados 
  FROM
    emple e 
      GROUP BY e.oficio
  ) c1
WHERE c1.nEmpleados>2
    GROUP BY c1.dept_no 
;

/* 15. Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades */
SELECT
  h.estanteria,
  SUM(h.unidades) suma 
FROM
  herramientas h 
    GROUP BY h.estanteria
;

/* 16. Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales) */
-- con totales
SELECT
  estanteria,MAX(c1.suma) 
FROM
  (
  SELECT
    h.estanteria,
    SUM(h.unidades) suma 
  FROM
    herramientas h 
      GROUP BY h.estanteria
  ) c1
;

-- sin totales
SELECT
  c2.estanteria 
FROM 
  (
    SELECT
      estanteria,MAX(c1.suma) 
    FROM
      (
        SELECT
          h.estanteria,
          SUM(h.unidades) suma 
        FROM
          herramientas h 
            GROUP BY h.estanteria
      ) c1
  ) c2
;


/* 17. Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital. */
SELECT
  m.cod_hospital,
  COUNT(*) nMedicos
FROM
  medicos m
    GROUP BY m.cod_hospital
    ORDER BY m.cod_hospital DESC
;

/* 18. Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene */
SELECT
  DISTINCT m.cod_hospital, m.especialidad
FROM
  medicos m
ORDER BY m.cod_hospital 
;

/*
  19. Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos
      (tendrás que partir de la consulta anterior y utilizar GROUP BY)
*/
SELECT
  m.cod_hospital,
  m.especialidad,
  COUNT(m.especialidad) nMedicos 
FROM
  medicos m
    GROUP BY m.cod_hospital,m.especialidad
;

/* 20. Obtener por cada hospital el número de empleados */
SELECT
  p.cod_hospital,
  COUNT(*) nEmpleados 
FROM
  personas p 
    GROUP BY p.cod_hospital
;

/* 21. Obtener por cada especialidad el número de trabajadores */
SELECT
  p.funcion especialidad,
  COUNT(*) nTrabajadores
FROM
  personas p
    GROUP BY p.funcion
;

/* 22. Visualizar la especialidad que tenga más médicos */
SELECT c1.especialidad 
FROM 
(SELECT
  MAX(c1.nMedicos) maximo
FROM 
  (
    SELECT
      m.especialidad,
      COUNT(*) nMedicos 
    FROM
      medicos m 
        GROUP BY m.especialidad
  )c1
) c2
JOIN
(
 SELECT
   m.especialidad,
   COUNT(*) nMedicos 
 FROM
   medicos m 
     GROUP BY m.especialidad
)c1 ON c2.maximo = c1.nMedicos
;

/* 23. ¿Cuál es el nombre del hospital que tiene mayor número de plazas? */
SELECT
  h.nombre 
FROM 
  hospitales h
JOIN (
  SELECT
    MAX(h.num_plazas) maximo 
  FROM hospitales h
 ) c1 ON h.num_plazas = c1.maximo
;

/* 24. Visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería */
SELECT
  h.estanteria
FROM
  herramientas h
    GROUP BY h.estanteria
    ORDER BY h.estanteria
;

/* 25. Averiguar cuántas unidades tiene cada estantería */
SELECT
  h.estanteria,
  SUM(h.unidades) suma
FROM
  herramientas h
    GROUP BY h.estanteria
;

/* 26. Visualizar las estanterías que tengan más de 15 unidades */
SELECT
  h.estanteria,
  SUM(h.unidades) nUnidades
FROM
  herramientas h
    GROUP BY h.estanteria
    HAVING nUnidades > 15
;

/* 27. ¿Cuál es la estantería que tiene más unidades? */
SELECT
  c3.estanteria
FROM (SELECT
    MAX(c1.suma) maximo
  FROM (SELECT
      h.estanteria,
      SUM(h.unidades) suma
    FROM herramientas h
    GROUP BY h.estanteria) c1) c2
  JOIN (SELECT
      h.estanteria,
      SUM(h.unidades) suma
    FROM herramientas h
    GROUP BY h.estanteria) c3
    ON c3.suma = c2.maximo
;

/* 28. A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado */
SELECT d.dept_no,
       d.dnombre 
FROM depart d 
  LEFT JOIN emple e ON d.dept_no = e.dept_no 
WHERE e.emp_no IS NULL
;

/* 
  29. Mostrar el número de empleados de cada departamento. 
      En la salida se debe mostrar también los departamentos que no
      tienen ningún empleado
*/
SELECT DISTINCT
  d.dept_no,
  COUNT(e.emp_no) nEmpleados
FROM depart d
LEFT JOIN emple e ON d.dept_no = e.dept_no
  GROUP BY d.dept_no
;

/* 
  30. Obtener la suma de salarios de cada departamento, mostrando las columnas DEPT_NO, SUMA DE SALARIOS y DNOMBRE.
      En el resultado también se deben mostrar los departamentos que no tienen asignados empleados
*/
SELECT
  d.dept_no,
  SUM(e.salario) suma_de_salarios,
  d.dnombre
FROM depart d
LEFT JOIN emple e ON d.dept_no = e.dept_no
  GROUP BY d.dept_no
;

/*
  31. Utilizar la función IFNULL en la consulta anterior para que en el caso de que un departamento no tenga empleados,
      aparezca como suma de salarios el valor 0
*/
SELECT
  d.dept_no,
  IFNULL(SUM(e.salario),0) suma_de_salarios,
  d.dnombre
FROM depart d
LEFT JOIN emple e ON d.dept_no = e.dept_no
  GROUP BY d.dept_no
;

/*
  32. Obtener el número de médicos que pertenecen a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE y
      NÚMERO DE MÉDICOS. En el resultado deben aparecer también los datos de los hospitales que no tienen médicos
*/
SELECT
  h.cod_hospital,
  h.nombre,
  COUNT(m.cod_hospital) numero_de_medicos
FROM
  hospitales h
LEFT JOIN medicos m ON h.cod_hospital = m.cod_hospital
  GROUP BY m.cod_hospital
    ORDER BY h.cod_hospital
;